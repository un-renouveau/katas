var assert = require('assert');
const bowling= require('../bowling');

describe("programme qui donne les resultats d'une partie fini de bowling.", function(){
    it("Should return result of tab", function(){
        const resultTabSolo = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        assert.strictEqual(bowling(resultTabSolo), 0)
    });
    it("Should return resultat additionner", function(){
        const resultTabSolo = [0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 0, 0]
        assert.strictEqual(bowling(resultTabSolo), 8)
    });
    it("Should return resultat additionner avec spare", function(){
        const resultTabSolo = [0, 0, 0, 0, 0, 0, 8, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        assert.strictEqual(bowling(resultTabSolo), 12)
    });
    it("Should return resultat additionner avec spare", function(){
        const resultTabSolo = [0, 0, 0, 0, 0, 0, 8, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        assert.strictEqual(bowling(resultTabSolo), 12)
    });
    it("Should return resultat additionner avec strike", function(){
        const resultTabSolo = [0, 0, 0, 0, 0, 0, 10, 3, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        assert.strictEqual(bowling(resultTabSolo), 20)
    });
    it("Should return resultat additionner avec 2 strikes", function(){
        const resultTabSolo = [0, 0, 0, 0, 0, 0, 10, 10, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        assert.strictEqual(bowling(resultTabSolo), 38)
    });
    it("Should return resultat additionner avec 3 strikes ", function(){
        const resultTabSolo = [0, 0, 0, 0, 0, 0, 10, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0]
        assert.strictEqual(bowling(resultTabSolo), 60)
    });
    it("Should return resultat additionner avec 3 strikes et autres points ", function(){
        const resultTabSolo = [0, 0, 0, 0, 0, 0, 10, 10, 10, 2, 1, 0, 0, 0, 0, 0, 0]
        assert.strictEqual(bowling(resultTabSolo), 68)
    });
    it("Should return resultat additionner avec melange de strike et spare ", function(){
        const resultTabSolo = [8, 2, 1, 0, 0, 0, 10, 10, 10, 2, 1, 0, 0, 0, 10, 10, 10, 10]
        assert.strictEqual(bowling(resultTabSolo), 130)
    });

    it("Should return ", function(){
        const resultTabSolo = [8, 2, 10, 0, 9, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        assert.strictEqual(bowling(resultTabSolo), 52)
    });
});

/*
8, 2,  -> 11
1, 0,  -> 1
0, 0, 
10,    -> 30
10,    -> 22
10,    -> 13
2, 1,  -> 3
0, 0, 
0, 10, -> 20
10,    -> 30

10,
10



-----
8, 2,  10 + 10
10,    10 + 0 + 9
0, 9,  9
1, 3,  4
0, 0, 
0, 0, 
0, 0, 
0, 0, 
0, 0, 
0, 0


8, 2,  10 + 10
10, 0, 10 + 9
9, 1,  10 + 3
3, 0,  3
0, 0, 
0, 0, 
0, 0, 
0, 0, 
0, 0


*/