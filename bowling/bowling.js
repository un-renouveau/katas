function bowling(pinsDown){
    let score = 0;
    let frames = 0;
    for(i=0; i < pinsDown.length -1 && frames < 10; i++){
        if(pinsDown[i] == 10){
            score += pinsDown[i] + pinsDown[i+1] + pinsDown[i+2];
        } else if (pinsDown[i] + pinsDown[i+1] == 10){
            score += pinsDown[i] + pinsDown[i+1] + pinsDown[i+2];
            i++;
        } else {
            score += pinsDown[i] + pinsDown[i+1];
            i++;
        }
        frames ++;
    }
    return score;
}

module.exports = bowling;

