var assert = require('assert');
const fizzBuzz2 = require('../fizzBuzz2');

describe("New version FizzBuzz with one Return !", function(){
    it("Should return the given number.", function(){
        assert.strictEqual(fizzBuzz2(4), 4);
    });
    it("Should return Fizz when the number is multiple of 3.", function(){
        assert.strictEqual(fizzBuzz2(21), "Fizz");
    });
    it("Should return Buzz when the number is multiple of 5.", function(){
        assert.strictEqual(fizzBuzz2(40), "Buzz");
    });
    it("Should return FizzBuzz when the number is multiple of 3 and 5.", function(){
        assert.strictEqual(fizzBuzz2(15), "FizzBuzz");
    });
})