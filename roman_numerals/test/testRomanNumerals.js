var assert = require('assert');
const romanNumerals = require('../romanNumerals');

describe('Roman numerals', function(){
    describe('the first steps, addition vocabulary.', function(){
        it('Should return 1 when I present.', function(){
            assert.deepEqual(romanNumerals("I"), (1));
        });
        it('Shoul return 5 when V present.', function(){
            assert.deepEqual(romanNumerals("V"), (5));
        });
        it('Should return 10 when X present.', function(){
            assert.deepEqual(romanNumerals("X"), 10);
        })
        it('Should return 5 0 when L present.', function(){
            assert.deepEqual(romanNumerals("L"), 50);
        })
        it('Should return 100 when C present.', function(){
            assert.deepEqual(romanNumerals("C"), 100);
        })
        it('Should return 500 when D present.', function(){
            assert.deepEqual(romanNumerals("D"), 500);
        })
        it('Should return 1000 when M present.', function(){
            assert.deepEqual(romanNumerals("M"), 1000);
        })
    })
    describe('The second step, addition of the addtion.', function(){
        it('Should additional split romanSplit', function(){
            assert.deepEqual(romanNumerals('II'), 2)
        })
        it('Should return 3 when III present.', function(){
            assert.deepEqual(romanNumerals('III'), 3);
        })
        it('Should return 2002 when MMII present.', function(){
            assert.deepEqual(romanNumerals('MMII'), 2002);
        })
    })
    describe('The second three, addtion of subtraction.', function(){
        describe('Exception handling of 4', function(){
            it('Should return 4 when IV present.', function(){
                assert.deepEqual(romanNumerals('IV'), 4);
            })
            it('Should return 24 when XXIV present', function(){
                assert.deepEqual(romanNumerals('XXIV'), 24);
            })
            it('Should return 40 when XL present', function(){
                assert.deepEqual(romanNumerals('XL'), 40);
            })
            it('Should return 400 when CD present', function(){
                assert.deepEqual(romanNumerals('CD'), 400);
            })
        })
        describe('Exception handling of 9', function(){
            it('Should return 9 when IX present.', function(){
                assert.deepEqual(romanNumerals('IX'), 9);
            })
            it('Should return 59 when LIX present', function(){
                assert.deepEqual(romanNumerals('LIX'), 59);
            })
            it('Should return 90 when XC present', function(){
                assert.deepEqual(romanNumerals('XC'), 90);
            })
            it('Should return 900 when CM present', function(){
                assert.deepEqual(romanNumerals('CM'), 900);
            })
        })
    })
    describe('The second four, test Limit.', function(){
        it('Should return 4 888 when MMMMDCCCLXXXVIII present.', function(){
            assert.deepEqual(romanNumerals('MMMMDCCCLXXXVIII'), 4888);
        })
    });
});