function romanNumerals(roman){
    const unit = { 'I':1 , 'V':5, 'X':10, 'L':50, 'C':100, 'D':500, 'M':1000 };
    let romanSplit = roman.split('');
   
    if(romanSplit.length > 1){
        let accuNumeral = 0;
        for(let i = 0; i < romanSplit.length; i++){
            if(unit[romanSplit[i]] < unit[romanSplit[i + 1]]){
                accuNumeral += unit[romanSplit[i + 1]] - unit[romanSplit[i]];
                i ++;
            }else{
                accuNumeral += unit[romanSplit[i]];
            }
        }
        return accuNumeral;
    }
    return unit[roman];
};

module.exports = romanNumerals;
