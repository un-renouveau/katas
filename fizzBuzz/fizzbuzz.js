function fizzBuzz(numeral){
    if(numeral % 5 == 0 && numeral % 3 == 0)
        return "FizzBuzz";
    if(numeral % 3 == 0)
        return "Fizz";
    if(numeral % 5 == 0)
        return "Buzz";

    return numeral;
};

module.exports = fizzBuzz;
121