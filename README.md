# Katas

## Le principe de ce repository :

### Dans un premier temps :
- S'amuser à découvir le kata en question.
- Le finir de la manière la plus simple en TDD.
- Utilisation des branches pour un Kata
- Faire des pull request pour faire une revue de mon code.

### Dans un second temps : 
- Refaire le Kata avec des contraintes suplémentaires.
- Explorer les principales notions explicité dans [le catalogue de refactoring](https://refactoring.com/catalog/) 
- L'utilisation de Clean code, différent pour chanque language.
- Utiliser/ manipuler au besoin des objets simple pour certains Katas.

### Dans un 3 ème temps :
- Voir et tester certaines architecture de code (voir les différents rangement). 
- Voir et tester certains design partterns (pour des utilisations spécifiques.)
