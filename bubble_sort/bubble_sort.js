function bubble_sort(tab){
    for(i=0; i < tab.length-1; i++){
         for(j=0; j< tab.length-1-i; j++){
            if(tab[j] > tab[j+1]){
                tmp = tab[j];
                tab[j] = tab[j+1];
                tab[j+1] = tmp;
            }
        }
    } 
}

var tab = [12, 1, 74, 5, 17, 20, 0];
console.log("tab avant bubble sort :", tab);
bubble_sort(tab);
console.log("tab après bubbule sort :", tab);
