function bubble_sort2(tab){
    table_size = tab.length;

    for(i = 0; i < table_size - 1; i++){
        for(j = 0; j < table_size - 1-i; j++){
            if(tab[j] > tab[j + 1]){
                tmp = tab[j];
                tab[j] = tab[j + 1];
                tab[j + 1] = tmp;
            }
        }
    }
    return tab; 
}

module.exports = bubble_sort2;