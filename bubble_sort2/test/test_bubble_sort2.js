var bubble_sort2 = require('../bubble_sort2');

var assert = require('assert');
describe('Bubble sort', function () {
  describe('Test limits of the sort', function () {
    it('Should sort the table with two elements.', function () {
      assert.deepEqual(bubble_sort2([34, 2]), [2, 34]);
    });
    it('Should sort all the elements.', function(){
      assert.deepEqual(bubble_sort2([23, 12, 30, 36, 5, 3, 0]), [0, 3, 5, 12, 23, 30, 36]);
    })
    it('Should sort a table sort the other way around.', function(){
      assert.deepEqual(bubble_sort2([80, 79, 66, 52, 45, 20, 0]), [0, 20, 45, 52, 66, 79, 80]);
    })
    it('Should not sort the table.', function() {
      assert.deepEqual(bubble_sort2([12, 12, 12, 12]), ([12, 12, 12, 12]));
    })
    it('Should return the array to an element.', function() {
      assert.deepEqual(bubble_sort2([512]), [512]);
    })
  });
});