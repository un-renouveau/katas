function bowling(tab){
    let score = 0;
    for(let i=0; i < tab.length; i++){
        score += quilleCourante(tab, i);
        score += spare(tab, i);
    }
    return score;
}

module.exports = bowling;


function spare(tab, i) {
    if (tab[i - 1] + tab[i - 2] == 10) {
        return tab[i];
    }
    return 0;
}

function quilleCourante(tab, i){
    return tab[i];
}
/*
 Strike = [10, 2]

i = 8;
score = 8 
i = 2
score = 10
i = 1 
score = 11
score = 12

*/