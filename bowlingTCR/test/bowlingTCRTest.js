var assert = require('assert');
const bowling= require('../bowlingTCR');

describe("programme qui donne les resultats d'une partie fini de bowling.", function(){
    it("devrait me retourner le résultat d'une partie de bouling", function(){
        const tab = [];
        assert.strictEqual(bowling(tab), 0);
    })
    it("devrait me retourner le résultat des scores d'une partie de bouling", function(){
        const tab = [1, 2];
        assert.strictEqual(bowling(tab), 3);
    })
    it("devrait me retourner le résultat des scores avec 1 spare", function(){
        const tab = [8, 2, 1];
        assert.strictEqual(bowling(tab), 12);
    })
});